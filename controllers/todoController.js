const { Todo, sequelize } = require('../models');
const { Sequelize } = require('sequelize');
const errorResponse = require('../utils/errorResponse');
const successResponse = require('../utils/successResponse');

class TodoController {
  static async getAllTodos(req,res) {
    try {
        const todos = await Todo.findAll();
        successResponse.withData(res, todos, 200);
    } catch (error) {
        errorResponse.serverError(res);
    }
  }

  static async getTodoById(req, res) {
    try {
      const todoId = req.params.id;
      const todo = await Todo.findOne({
        where: {
          id: todoId,
          deletedAt: null
        }
      });
  
      if (!todo) {
        errorResponse.notFoundError(res, 'Todo not found');
      } else {
        successResponse.withData(res, todo, 200);
      }
    } catch (error) {
      errorResponse.serverError(res);
    }
  }
  

  static async createTodo(req, res) {
    try {
        const { title } = req.body;
        const newTodo = await Todo.create({ title });
        successResponse.withData(res, newTodo, 201);
    } catch (error) {
        errorResponse.badRequestError(res);
    }
  }

  // soft delete
  static async deleteTodo(req, res) {
    try {
      const todoId = req.params.id;
      const todo = await Todo.findOne({
        where: {
          id: todoId,
          deletedAt: null
        }
      });

      if (!todo) {
        errorResponse.notFoundError(res, 'Todo not found');
      } else {
        const updateQuery = `
          UPDATE "Todos"
          SET "deletedAt" = :date
          WHERE "id" = :id
        `;

        const replacements = {
          date: new Date(),
          id: todoId,
        };

        await sequelize.query(updateQuery, {
          replacements,
          type: Sequelize.QueryTypes.UPDATE,
        });

        successResponse.withMessage(res, 'Todo soft deleted successfully', 200);
      }
    } catch (error) {
      errorResponse.serverError(res);
    }
  }
}

module.exports = TodoController;
