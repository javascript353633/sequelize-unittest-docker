module.exports = {
  withMessage: (res, message, status) => {
    res.status(status).json({ message });
  },
  withData: (res, data, status) => {
    res.status(status).json({ data });
  },
};
