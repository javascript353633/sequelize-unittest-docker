module.exports = {
    serverError: (res) => {
      res.status(500).json({ error: 'Server error' });
    },
    notFoundError: (res, message) => {
      res.status(404).json({ error: message || 'Resource not found' });
    },
    badRequestError: (res, message) => {
      res.status(400).json({ error: message || 'Bad request' });
    },
  };  