const request = require('supertest');
const app = require('../app');
const { sequelize } = require('../models');

describe('Todo API', () => {
  afterAll(async () => {
    await sequelize.close();
  });

  it('should get all todos', async () => {
    const response = await request(app).get('/v1/todos');
    expect(response.statusCode).toBe(200);
  });

  it('should create a new todo', async () => {
    const response = await request(app)
      .post('/v1/todos')
      .send({ title: 'New Todo' });
    expect(response.statusCode).toBe(201);
  });

  it('should get a todo by ID', async () => {
    const todo = await request(app).post('/v1/todos').send({ title: 'Test Todo' });

    const response = await request(app).get(`/v1/todos/${todo.body.id}`);
    expect(response.statusCode).toBe(200);
    expect(response.body.title).toBe('Test Todo');
  });

  it('should soft delete a todo', async () => {
    const todo = await request(app).post('/v1/todos').send({ title: 'Todo to Delete' });
  
    const response = await request(app).delete(`/v1/todos/${todo.body.id}`);
    expect(response.statusCode).toBe(200);
  
    const deletedTodoResponse = await request(app).get(`/v1/todos/${todo.body.id}`);
    expect(deletedTodoResponse.statusCode).toBe(404);
    expect(deletedTodoResponse.body).toEqual({ error: 'Resource not found' });
  
    // Check if the todo is still present in the database but marked as deleted
    const dbTodo = await Todo.findByPk(todo.body.id);
    expect(dbTodo).not.toBeNull();
    expect(dbTodo.deletedAt).not.toBeNull();
  });  
});
