# Todos App with Docker

A simple Todos application with Docker for managing title and soft delete functionality.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- - [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [Endpoints](#endpoints)

## Introduction

This project is a basic Todos application that allows users to create, list, and perform soft delete operations on title using a Dockerized environment.

## Features

- Create new title
- List all title
- Get a title by ID
- Soft delete title (sets deletedAt to a timestamp)

## Getting Started

Follow these instructions to set up and run the Todos application locally using Docker.

### Prerequisites

You'll need the following tools installed on your system:

- Docker
- Docker Compose

### Installation

1. Clone the repository:

   ```sh
   git clone https://gitlab.com/javascript353633/sequelize-unittest-docker.git
   ```

2. Navigate to the project directory:

   ```sh
   cd sequelize-unittest-docker
   ```

3. Build and run the Docker containers:

   ```sh
   docker-compose build
   docker-compose up
   ```

## Usage

Once the containers are up and running, you can access the Todos in `http://localhost:3000`.

## Endpoints

- `GET /v1/todos`: Get a list of all title.
- `GET /v1/todos/:id`: Get a title by its ID.
- `POST /v1/todos`: Create a new title.
- `DELETE /v1/todos/:id`: Soft delete a title (sets deletedAt to a timestamp).

