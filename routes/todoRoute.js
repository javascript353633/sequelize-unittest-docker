const express = require('express');
const TodoRouter = express.Router();
const {TodoController} = require('../controllers/index');

TodoRouter.get('/todos', TodoController.getAllTodos);
TodoRouter.get('/todos/:id', TodoController.getTodoById);
TodoRouter.post('/todos', TodoController.createTodo);
TodoRouter.delete('/todos/:id', TodoController.deleteTodo);

module.exports = TodoRouter;