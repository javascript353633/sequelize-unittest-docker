const express = require('express');
const app = express();
const {TodoRouter} = require('./routes/index');

app.use(express.json());
app.use('/v1', TodoRouter);

app.use((err, res) => {
    console.error(err.stack);
    res.status(500).send('Something went wrong!');
});

module.exports = app;